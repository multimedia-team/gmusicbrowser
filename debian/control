Source: gmusicbrowser
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Antonio Radici <antonio@dyne.org>,
 Jackson Doak <noskcaj@ubuntu.com>
Build-Depends:
 debhelper-compat (= 9),
 markdown
Standards-Version: 3.9.8
Homepage: http://www.gmusicbrowser.org/
Vcs-Git: https://salsa.debian.org/multimedia-team/gmusicbrowser.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/gmusicbrowser

Package: gmusicbrowser
Architecture: all
Depends:
 gir1.2-gstreamer-1.0,
 libglib-object-introspection-perl,
 libgtk2-perl,
 libgtk2.0-0 (>= 2.6),
 ${misc:Depends},
 ${perl:Depends}
Recommends:
 libcairo-perl,
 libdigest-crc-perl,
 libgtk2-notify-perl,
 libgtk2-trayicon-perl,
 libhtml-parser-perl,
 libintl-perl,
 libio-compress-perl,
 liblocale-gettext-perl (>= 1.04),
 libnet-dbus-perl
Suggests:
 alsa-utils,
 libgnome2-wnck-perl,
 libgtk2-appindicator-perl,
 libgtk2-mozembed-perl,
 mpg321 | flac123 | ogg123,
 mplayer | mpv,
 vorbis-tools
Description: graphical jukebox for large music collections
 gmusicbrowser is a powerful graphical browser which supports libraries
 with a very large number of songs (over 10,000).
 .
 It can use multiple inputs and has native support for MP3, Ogg, and
 FLAC files. It also supports mass-renaming and mass-retagging of a song
 library, multiple genres per song, ratings, and customizable labels.
 .
 gmusicbrowser has a customizable window layout, and comes with plugins
 to use Last.fm, retrieve lyrics, or find album pictures and WebContext
 (using the Mozilla or WebKit engines to display the artist's page on
 Wikipedia and search for lyrics with Google).
